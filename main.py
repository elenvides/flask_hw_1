from flask import Flask, render_template


app = Flask(__name__)


@app.route("/get_apple")
def get_apple():
    return "Apple"


@app.route("/")
def get_main_page():
    return render_template('home.html', title="Home")


@app.route("/vegetables")
def get_vegetables_page():
    vegetables = ["beans", "carrot", "beetroot", "cucumber"]
    return render_template('vegetables.html', title="Vegetables", vegetables=vegetables)


@app.route("/fruits")
def get_fruits_page():
    fruits = ["melon", "apple", "strawberry", "grape"]
    return render_template('fruits.html', title="Fruits", fruits=fruits)


if __name__ == '__main__':
    app.run(debug=True)
